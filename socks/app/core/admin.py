from django.contrib import admin

# Register your models here.
from core.models import *

class CoreUserAdmin(admin.ModelAdmin):
    list_display = ('user.username')

class SocksListAdmin(admin.ModelAdmin):
    list_display = ('hostname')

class TariffAdmin(admin.ModelAdmin):
    list_display = ('name')

admin.site.register(CoreUser)
admin.site.register(SocksList)
admin.site.register(Tariff)