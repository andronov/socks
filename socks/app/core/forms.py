# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.forms import *
from django import forms
from core.models import CoreUser


class CoreUserForm(forms.ModelForm):
    username = forms.CharField(max_length=20, label=u'username')
    email = forms.EmailField(max_length=100, label=u'Email')
    password = forms.CharField(max_length=12, label=u'password')
    icq = forms.CharField(max_length=9, label=u'ICQ', required=False)
    jabber = forms.CharField(label=u'Jabber', required=False)

    class Meta:
        model = CoreUser
        exclude = ['user', 'account', 'vip_status', 'proxy_list_size', 'unused_proxies', 'opt_till', 'balance']

    def clean_email(self):
        email = self.cleaned_data.get('email')

        try:
            current_email = self.instance.user.user.email
            if current_email == email:
                return email
        except:
            pass

        if email and User.objects.filter(email=email).count():
            raise forms.ValidationError(u'Email addresses must be unique.')
        return email

    def __init__(self, *args, **kwargs):
        super(CoreUserForm, self).__init__(*args, **kwargs)

        for field in self.base_fields:
            label = self.fields[field].label
        self.fields['username'].widget.attrs['placeholder'] = 'Username'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['jabber'].widget.attrs['placeholder'] = 'Jabber'
        self.fields['icq'].widget.attrs['placeholder'] = 'ICQ'
        self.fields['password'].widget.attrs['placeholder'] = 'Password'