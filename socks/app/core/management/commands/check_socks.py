# -*- coding: utf-8 -*-
from django.utils import timezone
from django.core.management.base import BaseCommand, CommandError
import math
from core.models import CoreUser, Tariff, SocksList
import urllib2
from core import socks
import threading
from core.sockshandler import SocksiPyHandler
from socks.settings import FETCH_TIMEOUT


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
      sockss = SocksList.objects.all()
      coun = SocksList.objects.all().count()
      summ = float(coun)/100
      sum = math.ceil(summ)
      def pro(n,tet):

        for sock in tet[n]:
            try:
               proxy_opener = urllib2.build_opener(SocksiPyHandler(socks.PROXY_TYPE_SOCKS5, str(sock.ip), int(sock.port)))
               proxy_opener.open("http://yandex.ru", None, FETCH_TIMEOUT).read()
               good = SocksList.objects.get(id=sock.id)
               good.need_recheck = True
               good.uptime = timezone.now()
               good.save()
               print('yes')
            except:
               good = SocksList.objects.get(id=sock.id)
               good.need_recheck = False
               good.save()
               print('no')

      for t in range(100):
         tet = zip(*[iter(sockss)] * int(round(sum, 1)))

         worker = threading.Thread(target=pro, name="t1", args=[t, tet])


         worker.start()