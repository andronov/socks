# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from core.models import CoreUser, Tariff


class Command(BaseCommand):

    def handle(self, *args, **options):
        unused_user = CoreUser.objects.filter(account=1)
        for user in unused_user:
            limit = user.tariff.limit
            user.unused_proxies = limit
            user.save()
