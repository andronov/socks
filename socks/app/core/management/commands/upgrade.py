# -*- coding: utf-8 -*-
import socket
import time
import datetime
import threading
from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geoip import GeoIP
import re
import math
from core.models import SocksList
from multiprocessing import Process
import urllib2
from core import socks
from core.sockshandler import SocksiPyHandler
from socks.settings import FETCH_TIMEOUT


class Command(BaseCommand):

    def handle(self, *args, **options):
        g = GeoIP()
        f = open("socks_test.txt", "r").readlines()
        coun =f.__len__()
        summ = float(coun)/100
        sum = math.ceil(summ)

        def pro(n,tet):
          for line in tet[n]:
            lineArr = line.split(':')
            ip = lineArr[0]
            port = re.sub('\n', '', lineArr[1])
            itog = g.city(ip)
            country = itog['country_name']
            country_in = itog['country_code']
            if itog['city']:
               city = itog['city']
            else:
               city="unknown"
            try:
               hostname = socket.getfqdn(socket.gethostbyaddr(ip)[0])
            except:
               hostname = "..."
            try:
                proxy_opener = urllib2.build_opener(SocksiPyHandler(socks.PROXY_TYPE_SOCKS5, str(ip), int(port)))
                proxy_opener.open("http://yandex.ru", None, FETCH_TIMEOUT).read()
                need_recheck = True
            except:
                need_recheck = False
            date = datetime.datetime.now()
            obj, created = SocksList.objects.get_or_create(ip=ip, port=port,
                  defaults={'country': country,
                            'hostname': hostname,
                            'country_in':country_in,
                            'city': city,
                            'need_recheck':need_recheck,
                            'uptime': date,
                            'last_check': date})
            if created == False:
               SocksList.objects.filter(ip=ip, port=port).update(uptime=date,
                            last_check=date, need_recheck=need_recheck)


        for t in range(100):
            tet = zip(*[iter(f)] * int(round(sum, 1)))
            worker = threading.Thread(target=pro, name="t1", args=[t, tet])
            worker.start()
