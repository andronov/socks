# -*- coding: utf-8 -*-
from datetime import timedelta, datetime, date
import re
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone


class Tariff(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    limit = models.IntegerField( blank=True, null=True)
    price = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = u"Тариф"
        verbose_name_plural = u'Тарифы'

    def __unicode__(self):
        return unicode(self.name)

ACCOUNT_ACTIVE = 1
ACCOUNT_SUSPENDED = 0

STATUS_ENABLE = 1
STATUS_DISABLE = 2

ACCOUNT_CHOICES = (
    (ACCOUNT_SUSPENDED, 'suspended'),
    (ACCOUNT_ACTIVE, 'active'),
)


STATUS_CHOICES = (
    (STATUS_ENABLE, 'NO'),
    (STATUS_DISABLE, 'YES')
)






class SocksList(models.Model):
    hostname = models.CharField(max_length=255, blank=True, null=True)
    ip = models.CharField(max_length=555)
    port = models.IntegerField(blank=True, null=True)

    country = models.CharField(max_length=255, blank=True, null=True)
    country_in = models.CharField(max_length=255, blank=True, null=True)#Сокращенное название страны
    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    need_recheck = models.BooleanField(default=True)

    uptime = models.DateTimeField(blank=True, null=True)
    last_check = models.DateTimeField(blank=True, null=True)
    speed = models.CharField(max_length=255, blank=True, null=True, default=0)

    def __unicode__(self):
        return unicode(self.hostname)

    def uptime_beta(self):
        res = str(datetime.now(timezone.utc) - self.uptime)
        return res[0:-7]

    class Meta:
        verbose_name = u"Список соксов"
        verbose_name_plural = u'Список соксов'

class CoreUser(models.Model):
    user = models.OneToOneField(User, related_name='core_user')
    icq = models.IntegerField(max_length=20, blank=True, null=True)
    jabber = models.CharField(max_length=200, blank=True, null=True)
    account = models.IntegerField(choices=ACCOUNT_CHOICES, default=ACCOUNT_ACTIVE, blank=True, null=True)
    vip_status = models.IntegerField(choices=STATUS_CHOICES, default=STATUS_ENABLE, blank=True, null=True)
    proxy_list_size = models.IntegerField(max_length=1000, blank=True, null=True,default=25)
    unused_proxies = models.IntegerField(blank=True, null=True)
    opt_till = models.DateTimeField(default=timezone.now,blank=True, null=True)#дата платежа

    tariff = models.ForeignKey(Tariff, related_name='user_tarif', blank=True)
    new_tariff = models.ForeignKey(Tariff, related_name='user_new_tarif', blank=True, null=True)

    used = models.ManyToManyField(SocksList, related_name='user_used', blank=True, null=True)

    balance = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = u"Пользователь"
        verbose_name_plural = u'Пользователи'

    def __unicode__(self):
        return unicode(self.user.username)

    # def save(self, *args, **kwargs):
    #     self.code = _createHash()
    #     super(JournalUser, self).save()

    def first_name(self):
        return self.user.first_name

    def last_name(self):
        return self.user.last_name

    def email(self):
        return self.user.email

    def paid_till(self):
        return self.opt_till + timedelta(30)

    def residue(self):
        p = re.sub("^\s+|[*]|[$]|[€]|\n|\r|\s+$", '', self.tariff.price)
        return self.balance - int(p)
