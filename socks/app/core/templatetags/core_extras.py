# -*- coding: utf-8 -*-
import re
from django import template
from core.models import *

register = template.Library()

@register.filter(name='count_country')
def count_country(value):
    socks = SocksList.objects.filter(need_recheck=True).filter(country=value).count()
    return socks

@register.filter(name='count_city')
def count_city(value,arg):
    socks = SocksList.objects.filter(need_recheck=True).filter(country=arg, city=value).count()
    return socks

@register.filter(name='edit_host')
def edit_host(value):
    return re.sub(r"\d+", "*", value, flags=re.UNICODE)
