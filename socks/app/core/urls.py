# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from core.views import *
from django.contrib.auth.views import login, logout


urlpatterns = patterns('',
    # Task
    url(r'^$', login_required(JournalPage.as_view())),
    url(r'^list', login_required(result), name='result'),
    url(r'^search', login_required(search), name='search'),
    url(r'^result/(?P<ids>.*)/(?P<id>.*)/', login_required(search_obj), name='search_obj'),
    url(r'^result/(?P<id>.*)/', login_required(search_obj_country), name='search_obj_country'),
    url(r'^host/(?P<id>.*)/', login_required(host_view), name='host_view'),
    url(r'^profile/(?P<username>.*)/', login_required(profile), name='profile'),
    url(r'^password', login_required(edit_password), name='edit_password'),
    url(r'^tarif', login_required(choice_tarif), name='choice_tarif'),

    url(r'^audit/(?P<id>.*)/', login_required(audits_socks), name='audits_socks'),

    url(r'^accounts/login/$',  login_view, name='login_view'),
    url(r'^accounts/logout/$', logout_view, name='logout_view'),

    url(r'^registration$', registration, name='registration'),

)