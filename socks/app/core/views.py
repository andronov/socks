from datetime import timedelta, datetime, date
import re
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import AnonymousUser
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django .db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.generic.base import TemplateView
from core.forms import CoreUserForm
from core.models import *
from socks.settings import FETCH_TIMEOUT


class JournalPage(TemplateView):
    template_name = "search.html"

    def get_context_data(self, **kwargs):
        context = super(JournalPage, self).get_context_data(**kwargs)
        news = 'test'
        context['socks'] = SocksList.objects.filter(need_recheck=True).order_by('country')
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
        user = self.request.user
        smena(user)
        core_user = CoreUser.objects.get(user=user)
        paid_till = core_user.paid_till()
        context['paid_till'] = paid_till
        if self.request.user.is_authenticated():
            context['name'] = self.request.user.username
        return context

    def dispatch(self, *args, **kwargs):
        return super(JournalPage, self).dispatch(*args, **kwargs)

def smena(user):
    core_user = CoreUser.objects.get(user=user)
    if core_user.paid_till() == date.today():
            core_user.tariff = core_user.new_tariff
            tar = Tariff.objects.get(name=core_user.tariff.name)
            core_user.unused_proxies = tar.limit
            core_user.save()

def result(request):
    context = RequestContext(request)
    user = request.user
    context['user'] = CoreUser.objects.get(user=user)
    if request.POST:
        country_s = request.POST.getlist('country')
        city_s = request.POST.getlist('city')
        city_un = request.POST.getlist('city_un')
        request.session['country_s'] = country_s
        request.session['city_s'] = city_s
        request.session['city_un'] = city_un

        socks_list = None

        socks_list_city_un = []
        if city_s != '':
          if city_un != '':
             for t in city_un:
               try:
                  hs = SocksList.objects.get(Q(country__icontains=str(t.split(',')[0]), city__icontains=str(t.split(',')[1])))
                  socks_list_city_un.append(hs.id)
               except:
                  iu = SocksList.objects.filter(Q(country__icontains=str(t.split(',')[0]), city__icontains=str(t.split(',')[1])))
                  for ss in iu:
                    socks_list_city_un.append(ss.id)
        else:
            pass


        socks_list_city = []
        if city_s != '':
          for t in city_s:
             try:
                hs = SocksList.objects.get(Q(city__icontains=str(t)))
                socks_list_city.append(hs.id)
             except:
                iu = SocksList.objects.filter(Q(city__icontains=str(t)))
                for ss in iu:
                    socks_list_city.append(ss.id)


        socks_list_country = []
        if country_s != '':
          for t in country_s:
             try:
                hs = SocksList.objects.get(Q(country__icontains=str(t)))
                socks_list_country.append(hs.id)
             except:
                iu = SocksList.objects.filter(Q(country__icontains=str(t)))
                for ss in iu:
                    socks_list_country.append(ss.id)

        socks_list = SocksList.objects.filter(need_recheck=True).filter(Q(id__in=socks_list_city_un) |
                                            Q(id__in=socks_list_city) |
                                            Q(id__in=socks_list_country)
                                            ).order_by('country')

        paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
           socks = paginator.page(page)
        except PageNotAnInteger:
           socks  = paginator.page(1)
        except EmptyPage:
           socks = paginator.page(paginator.num_pages)
        context['socks'] = socks
        context['name'] = request.user.username
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
    else:
        country_s = ''
        if request.POST.getlist('country_s'):
            country_s = request.session['country_s']
        city_s = ''
        if request.POST.getlist('city'):
           city_s = request.session['city_s']
        city_un = ''
        if request.POST.getlist('city_un'):
           city_un = request.session['city_un']
        socks_list = None

        socks_list_city_un = []
        if city_s != '':
          if city_un != '':
             for t in city_un:
               try:
                  hs = SocksList.objects.get(Q(country__icontains=str(t.split(',')[0]), city__icontains=str(t.split(',')[1])))
                  socks_list_city_un.append(hs.id)
               except:
                  iu = SocksList.objects.filter(Q(country__icontains=str(t.split(',')[0]), city__icontains=str(t.split(',')[1])))
                  for ss in iu:
                    socks_list_city_un.append(ss.id)
        else:
            pass


        socks_list_city = []
        if city_s != '':
          for t in city_s:
             try:
                hs = SocksList.objects.get(Q(city__icontains=str(t)))
                socks_list_city.append(hs.id)
             except:
                iu = SocksList.objects.filter(Q(city__icontains=str(t)))
                for ss in iu:
                    socks_list_city.append(ss.id)


        socks_list_country = []
        if country_s != '':
          for t in country_s:
             try:
                hs = SocksList.objects.get(Q(country__icontains=str(t)))
                socks_list_country.append(hs.id)
             except:
                iu = SocksList.objects.filter(Q(country__icontains=str(t)))
                for ss in iu:
                    socks_list_country.append(ss.id)

        socks_list = SocksList.objects.filter(need_recheck=True).filter(Q(id__in=socks_list_city_un) |
                                            Q(id__in=socks_list_city) |
                                            Q(id__in=socks_list_country)
                                            ).order_by('country')

        paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size)  # Show 25 contacts per page

        page = request.GET.get('page')
        try:
           socks = paginator.page(page)
        except PageNotAnInteger:
           socks  = paginator.page(1)
        except EmptyPage:
           socks = paginator.page(paginator.num_pages)
        context['socks'] = socks
        context['name'] = request.user.username
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
    return render_to_response('result-search.html', context)

def search(request):
    context = RequestContext(request)
    user = request.user
    context['user'] = CoreUser.objects.get(user=user)

    if request.POST:
      try:
        country_s = request.POST.get('country')
        city_s = request.POST.get('city')
        host_s = request.POST.get('host')
        ip = request.POST.get('ip')
        request.session['country_s'] = country_s
        request.session['city_s'] = city_s
        request.session['ip'] = ip
        request.session['host_s'] = host_s
        socks_list_host = []
        if host_s != '':
            for h in host_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', h)
              if p != '':
               try:
                   hs = SocksList.objects.get(Q(hostname__icontains=p))
                   socks_list_host.append(hs.id)
               except:
                   iu = SocksList.objects.filter(Q(hostname__icontains=p))
                   for ss in iu:
                       socks_list_host.append(ss.id)
              else:
                  pass

        socks_list_ip = []
        if ip != '':
            for i in ip.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(ip__icontains=p))
                   socks_list_ip.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(ip__icontains=p))
                   for ss in iu:
                       socks_list_ip.append(ss.id)
              else:
                pass
        socks_list_coun = []
        if country_s != '':
            for i in country_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(country__icontains=p))
                   socks_list_coun.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(country__icontains=p))
                   for ss in iu:
                       socks_list_coun.append(ss.id)
              else:
                pass
        socks_list_city = []
        if city_s != '':
            for i in city_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(city__icontains=p))
                   socks_list_city.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(city__icontains=p))
                   for ss in iu:
                      socks_list_city.append(ss.id)
              else:
                pass


        socks_list = SocksList.objects.filter(need_recheck=True).filter(Q(id__in=socks_list_coun) |
                                            Q(id__in=socks_list_city)|
                                            Q(id__in=socks_list_host)|
                                            Q(id__in=socks_list_ip)
                                            ).order_by('country')

        paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size)  # Show 25 contacts per page

        page = request.GET.get('page')
        try:
           socks = paginator.page(page)
        except PageNotAnInteger:
           # If page is not an integer, deliver first page.
           socks = paginator.page(1)
        except EmptyPage:
           # If page is out of range (e.g. 9999), deliver last page of results.
           socks = paginator.page(paginator.num_pages)

        context['socks'] = socks
        context['name'] = request.user.username
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
      except:
        context['socks'] = None

    else:

        country_s = request.session['country_s']
        city_s = request.session['city_s']
        ip = request.session['ip']
        host_s = request.session['host_s']
        socks_list_host = []
        if host_s != '':
            for h in host_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', h)
              if p != '':
               try:
                   hs = SocksList.objects.get(Q(hostname__icontains=p))
                   socks_list_host.append(hs.id)
               except:
                   iu = SocksList.objects.filter(Q(hostname__icontains=p))
                   for ss in iu:
                       socks_list_host.append(ss.id)
              else:
                  pass

        socks_list_ip = []
        if ip != '':
            for i in ip.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(ip__icontains=p))
                   socks_list_ip.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(ip__icontains=p))
                   for ss in iu:
                       socks_list_ip.append(ss.id)
              else:
                pass
        socks_list_coun = []
        if country_s != '':
            for i in country_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(country__icontains=p))
                   socks_list_coun.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(country__icontains=p))
                   for ss in iu:
                       socks_list_coun.append(ss.id)
              else:
                pass
        socks_list_city = []
        if city_s != '':
            for i in city_s.split(','):
              p = re.sub("^\s+|[*]|\n|\r|\s+$", '', i)
              if p != '':
                try:
                   iu = SocksList.objects.get(Q(city__icontains=p))
                   socks_list_city.append(iu.id)
                except:
                   iu = SocksList.objects.filter(Q(city__icontains=p))
                   for ss in iu:
                      socks_list_city.append(ss.id)
              else:
                pass
        socks_list = SocksList.objects.filter(need_recheck=True).filter(Q(id__in=socks_list_coun) |
                                            Q(id__in=socks_list_city)|
                                            Q(id__in=socks_list_host)|
                                            Q(id__in=socks_list_ip)
                                            ).order_by('country')

        paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size)  # Show 25 contacts per page

        page = request.GET.get('page')
        try:
           socks = paginator.page(page)
        except PageNotAnInteger:
           # If page is not an integer, deliver first page.
           socks = paginator.page(1)
        except EmptyPage:
           # If page is out of range (e.g. 9999), deliver last page of results.
           socks = paginator.page(paginator.num_pages)

        context['socks'] = socks
        context['name'] = request.user.username
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()

    return render_to_response('result-search.html', context)

def search_obj(request,ids=None, id=''):
    context = RequestContext(request)
    if id == '':
        return Http404
    user = request.user
    socks_list = SocksList.objects.filter(country=ids, city=id,need_recheck=True)

    paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size)
    page = request.GET.get('page')
    try:
           socks = paginator.page(page)
    except PageNotAnInteger:
           socks = paginator.page(1)
    except EmptyPage:
           socks = paginator.page(paginator.num_pages)

    context['socks'] = socks
    context['name'] = request.user.username
    context['user'] = CoreUser.objects.get(user=user)
    context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()

    return render_to_response('result-search.html', context)

def search_obj_country(request,id=''):
    context = RequestContext(request)
    if id == '':
        return Http404
    user = request.user
    socks_list = SocksList.objects.filter(need_recheck=True, country=id)

    paginator = Paginator(socks_list, CoreUser.objects.get(user=user).proxy_list_size)
    page = request.GET.get('page')
    try:
           socks = paginator.page(page)
    except PageNotAnInteger:
           socks = paginator.page(1)
    except EmptyPage:
           socks = paginator.page(paginator.num_pages)


    context['socks'] = socks
    context['name'] = request.user.username
    context['user'] = CoreUser.objects.get(user=user)
    context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()

    return render_to_response('result-search.html', context)

def host_view(request, id=''):
    context = RequestContext(request)
    if id == '':
        return Http404
    user = request.user
    core = CoreUser.objects.get(user=user)
    sock = SocksList.objects.get(id=id)
    context['sock'] = sock
    context['core'] = core
    return render_to_response('host-view.html', context)

def audits_socks(request,id):
    context = RequestContext(request)
    if id == '':
        return Http404
    user = request.user
    import urllib2
    from core import socks
    from sockshandler import SocksiPyHandler
    try:

       sock = SocksList.objects.get(id=id)
       proxy_opener = urllib2.build_opener(SocksiPyHandler(socks.PROXY_TYPE_SOCKS5, str(sock.ip), int(sock.port)))
       proxy_opener.open("http://yandex.ru", None, FETCH_TIMEOUT).read()

       core = CoreUser.objects.get(user=user)
       core.unused_proxies = int(core.unused_proxies) - 1
       core.used.add(sock)
       core.save()
       context['sock'] = sock
       return render_to_response('audits.html', context)

    except:
        sock = SocksList.objects.get(id=id)
        sock.need_recheck = False
        sock.save()
        core = CoreUser.objects.get(user=user)
        core.unused_proxies = int(core.unused_proxies) - 1
        core.used.remove(sock)
        core.save()
        context['sock'] = sock
        return render_to_response('audits-error.html', context)
    

def profile(request, username):
    context = RequestContext(request)
    user = request.user

    context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
    context['user'] = CoreUser.objects.get(user=user)
    context['name'] = request.user
    if request.POST:
        inter = request.POST.get('list_size')
        core = CoreUser.objects.get(user=user)
        core.proxy_list_size = inter
        core.save()
        context['socks_count'] = SocksList.objects.filter(need_recheck=True).count()
        context['user'] = CoreUser.objects.get(user=user)
        context['name'] = request.user

    return render_to_response('profile.html', context)

def registration(request):
    context = RequestContext(request)
    context['coreuser_forms'] = CoreUserForm

    if request.POST:
        coreuser_form = CoreUserForm(request.POST)
        if coreuser_form.is_valid():
            coreuser = coreuser_form.save(commit=False)

            email = coreuser_form.cleaned_data.get('email')
            password = coreuser_form.cleaned_data.get('password')
            username = coreuser_form.cleaned_data.get('username')
            jabber = coreuser_form.cleaned_data.get('jabber')
            icq = coreuser_form.cleaned_data.get('icq')

            tariff = coreuser_form.cleaned_data.get('tariff')

            tarif = Tariff.objects.get(name=tariff)

            user = User()
            user.username = username
            user.email = email
            user.set_password(password)
            user.save()
            coreuser.user = user
            if jabber != '':
                coreuser.jabber = jabber
            else:
                coreuser.jabber = '0'
            if icq != '':
                coreuser.icq = int(icq)
            else:
                coreuser.icq = int(0)
            coreuser.tariff = tarif
            coreuser.unused_proxies = int(tarif.limit)
            coreuser.save()
            return HttpResponseRedirect("/")
        else:
            context['errors'] = True
            return render_to_response('registration/registration.html', context)

    return render_to_response('registration/registration.html', context)


def edit_password(request):
    context = RequestContext(request)
    user =request.user

    core_user = CoreUser.objects.get(user=user)
    if request.POST:
        password = request.POST.get('password')
        new_password = request.POST.get('new_password')
        user_good = authenticate(username=user.username, password=password)
        if user_good and new_password:
            user_good.set_password(new_password)
            user_good.save()
            context['success'] = True
        else:
            context['errors'] = True
        context['core_user'] = core_user
    context['core_user'] = core_user
    return render_to_response('registration/password.html', context)

def choice_tarif(request):
    context = RequestContext(request)
    user =request.user
    core_user = CoreUser.objects.get(user=user)
    tariff = Tariff.objects.all()
    if request.POST:
        tarif = request.POST.get('tarif')
        if tarif and int(tarif) == int(core_user.tariff.id):
            context['similar'] = True
        elif tarif and int(tarif) != int(core_user.tariff.id):
            t = Tariff.objects.get(id=int(tarif))
            core_user.new_tariff = t
            core_user.save()
            context['success'] = True
        context['core_user'] = core_user
    context['core_user'] = core_user
    context['tariff'] = tariff
    return render_to_response('choise_tarif.html', context)

def login_view(request):
    context = RequestContext(request)
    user =request.user
    from django.contrib.auth import login as auth_login
    if user.is_authenticated():
      return HttpResponseRedirect("/")
    else:
      if request.POST:
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            return HttpResponseRedirect("/")
        else:
            context['errors'] = True
    return render_to_response('registration/login.html', context)

def logout_view(request):
    from django.contrib.auth import logout as auth_logout
    auth_logout(request)
    return HttpResponseRedirect("/accounts/login/")
